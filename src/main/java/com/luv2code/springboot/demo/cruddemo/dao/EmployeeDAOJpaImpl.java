package com.luv2code.springboot.demo.cruddemo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.luv2code.springboot.demo.cruddemo.entity.Employee;

@Repository
public class EmployeeDAOJpaImpl implements EmployeeDAO {

	private EntityManager entityManager;
	
	// Constructor injection
	@Autowired
	public EmployeeDAOJpaImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<Employee> findAll() {
		
		// create query
		Query theQuery = entityManager.createQuery("from Employee");
		
		// execute the query and get result list
		List<Employee> employees = theQuery.getResultList();
		
		// return the result
		return employees;
	}

	@Override
	public Employee findById(int theId) {
		
		// get the employee 
		Employee theEmployee = entityManager.find(Employee.class, theId);
		
		// return the employee
		return theEmployee;
		
	}

	@Override
	public void save(Employee theEmployee) {

		// save or update the employee
		// merge mean if the id == 0 then insert/save else update 
		Employee dbEmployee = entityManager.merge(theEmployee);
		
		// update with id from db  .. so we can get generates id for save/insert
		theEmployee.setId(dbEmployee.getId());
	}

	@Override
	public void deleteById(int theId) {

		// delete object with primary key
		Query theQuery = entityManager.createQuery("delete from Employee where 	id=:employeeId");
		
		theQuery.setParameter("employeeId", theId);
		
		theQuery.executeUpdate();
	}

}
